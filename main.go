package main

import (
	"appbones/routes"
	"appbones/version"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	log.Printf(
		"Starting the service...\ncommit: %s, build time: %s, release: %s, branch: %s",
		version.Commit, version.BuildTime, version.Release, version.Branch,
	)

	mainPort := os.Getenv("PORT")
	if mainPort == "" {
		log.Fatal("Port is not set.")
	}

	router := routes.MakeRouter()
	mainSrv := &http.Server{
		Addr:    ":" + mainPort,
		Handler: router,
	}

	go func() {
		log.Fatal(mainSrv.ListenAndServe())
	}()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	killSignal := <-interrupt
	switch killSignal {
	case os.Interrupt:
		log.Println("Got SIGINT...")
	case syscall.SIGTERM:
		log.Println("Got SIGTERM...")
	}

	log.Println("The service is shutting down...")
	mainSrv.Shutdown(context.Background())
	log.Println("Done.")

}
