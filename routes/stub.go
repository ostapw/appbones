package routes

import (
	"fmt"
	"net/http"
)

func stubHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Println("Not implemented")
}
