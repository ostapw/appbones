package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestStubHandler(t *testing.T) {
	w := httptest.NewRecorder()
	stubHandler(w, nil)

	resp := w.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Status code is wrong. Have: %d, expected: %d.", resp.StatusCode, http.StatusOK)
	}
}
