package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHomeHandler(t *testing.T) {
	w := httptest.NewRecorder()
	homeHandler(w, nil)

	resp := w.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Status code is wrong. Have: %d, expected: %d.", resp.StatusCode, http.StatusOK)
	}
}
