package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMakeRouter(t *testing.T) {
	r := MakeRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	res, err := http.Get(ts.URL + "/")

	if err != nil {
		t.Fatal(err)
	}
	if res.StatusCode != http.StatusOK {
		t.Errorf("Status code for / is wrong. Have %d, expected %d.", res.StatusCode, http.StatusOK)
	}

	res, err = http.Post(ts.URL+"/", "text/plain", nil)
	if err != nil {
		t.Fatal(err)
	}
	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Status code for / is wrong. Have %d, expected %d.", res.StatusCode, http.StatusMethodNotAllowed)

	}
}
