package routes

import (
	"github.com/gorilla/mux"
	"github.com/heptiolabs/healthcheck"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"time"
)

var (
	customMetrics = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "appbones_custom_metrics_total",
		Help: "AppBones app custom metrics.",
	})
)

func MakeRouter() *mux.Router {
	router := mux.NewRouter()

	prometheus.MustRegister(customMetrics)

	go startMetrics()

	router.HandleFunc("/", homeHandler).Methods("GET")
	router.HandleFunc("/items/", stubHandler).Methods("GET")
	router.HandleFunc("/items/", stubHandler).Methods("POST")
	router.HandleFunc("/items/{id}", stubHandler).Methods("PUT")
	router.HandleFunc("/items/{id}", stubHandler).Methods("DELETE")
	router.HandleFunc("/items/{id}", stubHandler).Methods("GET")
	router.Handle("/metrics", promhttp.Handler())
	addHealthCheck(router)

	return router
}

func addHealthCheck(router *mux.Router) {
	health := healthcheck.NewHandler()
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	router.HandleFunc("/live", health.LiveEndpoint).Methods("GET")
	router.HandleFunc("/ready", health.ReadyEndpoint).Methods("GET")
}

func startMetrics() {
	ticker := time.NewTicker(5000 * time.Millisecond)

	for range ticker.C {
		t := time.Now()
		customMetrics.Set(float64(t.Second()))
	}
}
