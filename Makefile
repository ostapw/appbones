PROJECT=appbones
CURRENT_DIR=$(shell pwd)
BINARY = $(shell basename ${CURRENT_DIR} | tr "[:upper:]" "[:lower:]")
#GOOS = $(shell uname | tr "[:upper:]" "[:lower:]")
GOOS = linux
GOARCH = amd64

CONTAINER_IMAGE=docker.io/ostapw
VERSION?=0.0.1
COMMIT=$(shell git rev-parse HEAD)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
BUILD_TIME=$(shell date -u '+%Y-%m-%d_%H:%M:%S')

# Symlink into GOPATH
BUILD_DIR=${CURRENT_DIR}/build
BUILD_DIR_LINK=$(shell readlink ${BUILD_DIR})

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS = -ldflags "-X ${PROJECT}/version.Release=${VERSION} -X ${PROJECT}/version.Commit=${COMMIT} -X ${PROJECT}/version.Branch=${BRANCH} -X ${PROJECT}/version.BuildTime=${BUILD_TIME}"

DOCKER=docker

# Project environment variables
DB_USER=test
DB_PWD=test
DB_NAME=test
DB_HOST=127.0.0.1

# Build the project
all: link clean test binary

link:
	BUILD_DIR=${BUILD_DIR}; \
	BUILD_DIR_LINK=${BUILD_DIR_LINK}; \
	CURRENT_DIR=${CURRENT_DIR}; \
	if [ "$${BUILD_DIR_LINK}" != "$${CURRENT_DIR}" ]; then \
	    echo "Fixing symlinks for build"; \
	    rm -f $${BUILD_DIR}; \
	    ln -s $${CURRENT_DIR} $${BUILD_DIR}; \
	fi

binary: 
	cd ${BUILD_DIR}; \
	GOOS=${GOOS} GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BINARY} . ; \
	cd - >/dev/null

test: binary
	ABONES_DB_NAME=${DB_NAME} ABONES_DB_USER=${DB_USER} ABONES_DB_PWD=${DB_PWD} ABONES_DB_HOST=${DB_HOST} go test ./...

fmt:
	cd ${BUILD_DIR}; \
	go fmt $$(go list ./... | grep -v /vendor/) ; \
	cd - >/dev/null

clean:
	-rm -f ${BINARY}

docker: binary test
	cat Dockerfile.template | \
		sed -E "s/{{\.AppName}}/${BINARY}/g" \
    	> Dockerfile

	${DOCKER} build -t ${CONTAINER_IMAGE}/${BINARY}:${COMMIT} ./
	${DOCKER} tag  ${CONTAINER_IMAGE}/${BINARY}:${COMMIT} ${CONTAINER_IMAGE}/${BINARY}:latest

push: docker
	${DOCKER} push ${CONTAINER_IMAGE}/${BINARY}:latest


.PHONY: link binary fmt clean docker push test minikube

