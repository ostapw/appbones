FROM golang AS appbones


ENV PORT 8080
EXPOSE $PORT
ENV HEALTH_PORT 8086
EXPOSE $HEALTH_PORT

ARG app_env
ENV APP_ENV $app_env

COPY appbones /
CMD ["/appbones"]
	
