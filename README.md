# Appbones template app

To run and check you need a k8s cluster with ambassador gateway (https://www.getambassador.io/) in default namespace.
Use helm chart in appbones-app directory
helm install appbones-app

To check the app you have to check <external_ambassador_ip>/appbones/


To build the project you need to install:
- golang https://golang.org/
- gvt https://github.com/FiloSottile/gvt
- docker https://www.docker.com/
