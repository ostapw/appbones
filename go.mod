module appbones

require (
	github.com/beorn7/perks v0.0.0-20180321164747-3a771d992973
	github.com/golang/protobuf v0.0.0-20180919224659-7716a980bcee
	github.com/gorilla/handlers v0.0.0-20180914152756-7e0369fae54b // indirect
	github.com/gorilla/mux v0.0.0-20180903154305-9e1f5955c0d2
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/matttproud/golang_protobuf_extensions v1.0.1
	github.com/namsral/flag v0.0.0-20170814194028-67f268f20922 // indirect
	github.com/prometheus/client_golang v0.0.0-20180919114304-73edb9af667d
	github.com/prometheus/client_model v0.0.0-20180712105110-5c3871d89910
	github.com/prometheus/common v0.0.0-20180801064454-c7de2306084e
	github.com/prometheus/procfs v0.0.0-20180920065004-418d78d0b9a7
	golang.org/x/net v0.0.0-20180911220305-26e67e76b6c3 // indirect
)
